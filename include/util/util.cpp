#include "util.h"

std::vector<std::string> Split(std::string FileContent, char symbol) {
  std::vector<std::string> Lines;

  std::string newline;
  newline = "\n";
  int temp_acum = 0;
  int i = 0;

  for (; i < FileContent.size(); i++) {
    if (FileContent[i] == symbol) {
      std::string temp_string = FileContent.substr(temp_acum, i-temp_acum);
      Lines.push_back(temp_string);
      temp_acum = i+1;
    }
  }

  std::string temp_string = FileContent.substr(temp_acum, i - temp_acum);
  Lines.push_back(temp_string);

  return Lines;
}

std::vector<std::vector<int>> ReadIntegerValues(std::string Content) {
  std::vector<std::string> NewLines = Split(Content, '\n');
  std::vector<std::vector<int>> IntegerValues;

  for (int i=1; i < NewLines.size(); i++) {
    std::vector<std::string> CommaSeparated = Split(NewLines[i], ',');
    std::vector<int> values;

    for (int j=0; j < CommaSeparated.size(); j++) {
      values.push_back(std::stoi(CommaSeparated[j]));
    }

    IntegerValues.push_back(values);
  }

  return IntegerValues;
}

std::vector<std::string> Filter(std::vector<std::string> arr,  int xliminf, int xlimsup)  {
  std::vector<std::string> temp_arr;

  for (int i = xliminf; i < xlimsup; i ++) {
    temp_arr.push_back(arr[i]);
  }

  return temp_arr;
}

void ArrayReader(std::vector<std::string> arr) {
  for (int i = 0; i < arr.size(); i++ )  {
    std::cout << arr[i] << std::endl;
  }
}

void MatrixReader(std::vector<std::vector<int>> arr) {
  for (int i = 0; i < arr.size(); i++ )  {
    for (int j = 0; j < arr[i].size(); j++) {
      std::cout << arr[i][j] << " ";
    }

    std::cout << "\n";
  }
}

std::vector<Coordinates> GetVertices(std::vector<std::vector<int>> matrix) {
  std::vector<Coordinates> vertices;

  for (int i = 0; i < matrix.size(); i++) {
    vertices.push_back({x: matrix[i][1], y: matrix[i][2]});
  }

  return vertices;
}

std::vector<Triangle> GetTriangles(std::vector<Coordinates> coordinates, std::vector<std::vector<int>> indices) {
  std::vector<Triangle> triangles;
  std::array<Coordinates, 3> temp_arr;

  for (int i = 0; i < indices.size(); i++) {
    temp_arr = {{coordinates[indices[i][0]], coordinates[indices[i][1]], coordinates[indices[i][2]]}};
    triangles.push_back({temp_arr});
  }

  return triangles;
}
