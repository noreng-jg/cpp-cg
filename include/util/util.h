#ifndef UTIL_H
#define UTIL_H

#include <vector>
#include <iostream>
#include <array>

struct Coordinates {
  int x;
  int y;
};

struct Triangle {
  std::array<Coordinates, 3> indices;
};

std::vector<std::string> Split(std::string, char);
std::vector<std::vector<int>> ReadIntegerValues(std::string);
std::vector<std::string> Filter(std::vector<std::string>, int, int);
std::vector<Coordinates> GetVertices(std::vector<std::vector<int>>);
std::vector<Triangle> GetTriangles(std::vector<Coordinates>, std::vector<std::vector<int>>);
void ArrayReader(std::vector<std::string> arr);
void MatrixReader(std::vector<std::vector<int>> arr);

#endif
