#include "file.h"

File::File(std::string filename) {
  this->filename = filename;
  this->FileContent = "none";

  std::cout << "Calling file constructor ...\n";
  std::ifstream is(filename, std::ifstream::binary);

  if (is) {
    is.seekg(0, is.end);
    int length = is.tellg();
    is.seekg(0, is.beg);

    char * buffer = new char [length];

    std::cout << "reading " << length << " character ... ";

    is.read(buffer, length);

    if (is)
      std::cout << "all characters were read successfully. \n";
    else
      std::cout << "error: only " << is.gcount() << " could be read";
    is.close();

    std::string ret(buffer, is.gcount());

    ret.pop_back();

    this->FileContent = ret;

    delete[] buffer;
  }
}

std::string File::GetFileContent() {
  return this->FileContent;
};
