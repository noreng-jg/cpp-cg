#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <iostream>
#include <string>

class File {
  public:
  File(std::string);
  std::string GetFileContent();

  private:
    std::string filename;
    std::string FileContent;
};

#endif
