#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <functional>
#include "util.h"

#define XDMAX 500
#define YDMAX 500
#define ALT 500
#define WINDOW_POSITION 100

class Graphics {
  public:
  std::vector<Triangle> triangles_;
};

void WindowUpdate(GLsizei, GLsizei);
void init(void);
void DrawCircle(int x1, int y1, int raio, int t);
void point_circ(int x, int y, int x1, int y1, int t);

#endif
