#include "graphics.h"

void WindowUpdate(GLsizei w, GLsizei h) {
  if(h == 0)
    h = 1;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (w <= h)
    gluOrtho2D(0, XDMAX, 0, YDMAX*h/w);
  else
    gluOrtho2D(0, XDMAX*w/h, 0, YDMAX);
}

void init(void) {
  glClearColor(0, 0, 0, 0);
}

void point_circ(int x, int y, int x1, int y1, int t) {
  glBegin(GL_POLYGON);
    glVertex2i(x1+x, y1+y);
    glVertex2i(x1+y, y1+x);
    glVertex2i(x1+y, y1-x);
    glVertex2i(x1+x, y1-y);
    glVertex2i(x1-x, y1-y);
    glVertex2i(x1-y, y1-x);
    glVertex2i(x1-y, y1+x);
    glVertex2i(x1-x, y1+y);
  glEnd();
}


void DrawCircle(int x1, int y1, int raio, int t) {

  int x,y;
  double d;

  x = 0;
  y = raio;
  d = 5./4 - raio;

  point_circ(x, y, x1, y1, t);
  while (y > x) 
  {
    if (d < 0) 
      d += 2.0*x + 3.0;
    else 
      { 
       d += 2.0*(x-y) + 5;
       y--;
      }
    x++;
    point_circ(x, y, x1, y1, t);
  } 

} 
