CXX=g++

CFLAGS=$(CXX) -std=c++20

.RECIPEPREFIX = >

INC_FILE=include/file
INC_FILE_FLAGS= -I ./$(INC_FILE)

INC_UTIL=include/util
INC_UTIL_FLAGS = -I ./$(INC_UTIL)

INC_GRAPHICS=include/graphics
INC_GRAPHICS_FLAGS= -I ./$(INC_GRAPHICS)

OPENGL_FLAGS=-lGL -lglut -lGLU

INC_ALL_FLAGS=$(INC_FILE_FLAGS) $(INC_UTIL_FLAGS) $(INC_GRAPHICS_FLAGS)

HEADER_FLAGS=$(INC_FILE)/file.h $(INC_UTIL)/util.h $(INC_GRAPHICS)/graphics.h

# Main compilation and object contruction tasks

build: main.o file.o util.o graphics.o
> $(CFLAGS) file.o main.o graphics.o util.o -o bin/main $(OPENGL_FLAGS)

main.o: src/main.cpp $(HEADER_FLAGS) 
> $(CFLAGS) $(INC_ALL_FLAGS) -c src/main.cpp

file.o:
> $(CFLAGS) $(INC_FILE_FLAGS) -c $(INC_FILE)/file.cpp

util.o:
> $(CFLAGS) $(INC_UTIL_FLAGS) -c $(INC_UTIL)/util.cpp

graphics.o: util.o $(INC_UTIL)/util.h
> $(CFLAGS) $(INC_GRAPHICS_FLAGS) $(INC_UTIL_FLAGS) -c $(INC_GRAPHICS)/graphics.cpp $(OPENGL_FLAGS)

# Test related tasks

test.o: tests/main.cpp $(HEADER_FLAGS)
> $(CFLAGS) $(INC_ALL_FLAGS) -c tests/main.cpp -o test.o 

test: test.o file.o util.o
> $(CFLAGS) file.o test.o util.o -o bin/test

clean:
> rm *.o
> rm bin/main

run:
> ./bin/main
