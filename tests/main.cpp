#include <iostream>
#include <array>
#include "util.h"

#define RED_BUFFER "\033[1m\033[31m"
#define GREEN_BUFFER "\033[1m\033[32m"
#define RESET "\033[0m"

typedef struct TestSpec {
  std::string description;
  std::string error;
} tspec;

int testcounter = 0;
int totalfailed = 0;
int totalpassed = 0;

template <typename T>
void TestFunc(T result, T expect, tspec* to) {
  testcounter++;

  std::cout << "=======================================================================\n" << std::endl;
  std::cout << testcounter << " : Running test : " << to->description <<  " ...\n\n" << std::endl;
  std::cout << GREEN_BUFFER;

  if (result != expect) {
    std::cout << RED_BUFFER;
    std::cout << "has failed: " << to->error << std::endl;
    std::cout << RESET;
    totalfailed++;
  } else {
    std::cout << "has passed ..."<< std::endl;
    totalpassed++;
  }

  std::cout << RESET;
  std::cout << "\n=======================================================================\n" << std::endl;
}

void PrintStatistics() {
  std::cout << "=======================================================================\n" << std::endl;

  std::cout << "----FINALIZED ----\n" << std::endl;
  std::cout << testcounter << " were run " << GREEN_BUFFER  << totalpassed <<" have passed " << RED_BUFFER << totalfailed << " have failed. " << RESET << std::endl;
  std::cout << "\n=======================================================================\n" << std::endl;
}

int main() {
  {
    std::string csv = "c1,c2,c3\n1,2,3\n4,5,6\n7,8,9";
    std::vector<std::vector<int>> result = ReadIntegerValues(csv);
    std::vector<std::vector<int>> expected = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};

    TestFunc(
        result,
        expected,
        new tspec{
          "Utils: Should convert string data to integer vector",
          "Error",
    });
  }

  {
    std::vector<std::vector<int>> vector = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    std::vector<Coordinates> result = GetVertices(vector);
    for (int i = 0; i < result.size(); i++) {
      std::cout << result[i].x <<  " " << result[i].y << std::endl;
    }
  }

  PrintStatistics();

  return 0;
}
