/*
 ===============================================================
  Author: Vagner Nornberg
  Subject: Computer Graphics - Electrical Engineering
  Att: 01
  ==============================================================
*/

#include "file.h"
#include "graphics.h"

std::vector<Triangle> triangles_;
std::vector<std::vector<int>> rectangle_;
std::vector<std::vector<int>> circle_1;
std::vector<std::vector<int>> circle_2;

bool debug = true;
int scale_factor = 5;
int shift_right = 120;
int shift_up = 120;

void DrawTriangulizedPolygon(std::vector<Triangle> triangles_)  {
  glColor3f(1, 1, 0);
  glLineWidth(3);
  int counter = 0;

    for (int i =0; i < triangles_.size(); i++)  {
      counter ++;
      if (debug) {
        std::cout <<  " " << std::endl;
        std::cout << triangles_.size() << " " << counter<< std::endl;
        std::cout << " " << triangles_[i].indices[0].x << " " << triangles_[i].indices[0].y;
        std::cout << " " << triangles_[i].indices[1].x << " " << triangles_[i].indices[1].y;
        std::cout << " " << triangles_[i].indices[2].x << " " << triangles_[i].indices[2].y;
      }

      glBegin(GL_POLYGON);
        glVertex2i(shift_right + triangles_[i].indices[0].x * scale_factor,
            triangles_[i].indices[0].y * scale_factor + shift_up);

        glVertex2i(shift_right + triangles_[i].indices[1].x * scale_factor,
            triangles_[i].indices[1].y * scale_factor + shift_up);

        glVertex2i(shift_right + triangles_[i].indices[2].x * scale_factor,
            triangles_[i].indices[2].y * scale_factor + shift_up);
      glEnd();
    }

}

void DrawRectangle() {
  glLineWidth(3);

  glBegin(GL_POLYGON);

  for (int i = 0; i < rectangle_.size(); i++) {
    glVertex2i(shift_right + rectangle_[i][1] * scale_factor, rectangle_[i][2] * scale_factor + shift_up);
  }

  glEnd();
}

void DrawFigure() {
  glClear(GL_COLOR_BUFFER_BIT);

  DrawTriangulizedPolygon(triangles_);

  glColor3f(0, 1, 0);
  DrawRectangle();

  glColor3f(1, 0, 0);
  DrawCircle(circle_1[0][1]*scale_factor + shift_right,
      circle_1[0][2]*scale_factor + shift_up, scale_factor*circle_1[0][3], 0);

  glColor3f(0, 1, 0);
  DrawCircle(circle_2[0][1]*scale_factor + shift_right,
      circle_2[0][2]*scale_factor + shift_up, scale_factor*circle_2[0][3], 0);


  glFlush();
}

int main(int argc, char**argv) {

  // Bidimensional primitives call and Data Processing

  File file("data/yellow-all.csv");
  std::vector<std::vector<int>> Matrix = ReadIntegerValues(file.GetFileContent());
  std::vector<Coordinates> vertices1 = GetVertices(Matrix);

  File file2("data/yellow-triangles.csv");
  std::vector<std::vector<int>> indices = ReadIntegerValues(file2.GetFileContent());

  File file3("data/green-rect.csv");
  rectangle_ = ReadIntegerValues(file3.GetFileContent());

  File file4("data/red-circle.csv");
  circle_1 = ReadIntegerValues(file4.GetFileContent());

  File file5("data/green-circle.csv");
  circle_2 = ReadIntegerValues(file5.GetFileContent());

  std::cout << indices.size() << std::endl;

  triangles_ = GetTriangles(vertices1, indices);

  std::cout << triangles_.size() << std::endl;

  /* OpenGL Related calls */

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(XDMAX, YDMAX);
  glutInitWindowPosition(WINDOW_POSITION, WINDOW_POSITION);
  glutCreateWindow("OPENGL - Att-1-Vagner_Nornberg");
  glutDisplayFunc(DrawFigure);
  glutReshapeFunc(WindowUpdate);
  init();
  glutMainLoop();

  return 0;
}
